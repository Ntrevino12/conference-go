import requests
from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY
import json


def get_weather(city, state):
    iso = "3166-2"
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{iso}&limit=1&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)

    loc = json.loads(response.content)

    lat = loc[0]["lat"]
    lon = loc[0]["lon"]

    new_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    response2 = requests.get(new_url)

    new_loc = json.loads(response2.content)

    weath = {
        "description": new_loc["weather"][0]["description"],
        "tempature": new_loc["main"]["temp"],
    }
    try:
        return weath
    except:
        return {
            "discription": None,
            "tempature": None,
        }


def get_pic(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, headers=headers, params=params)

    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}
