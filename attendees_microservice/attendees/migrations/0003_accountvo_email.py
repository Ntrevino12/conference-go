# Generated by Django 4.1.6 on 2023-02-15 22:28

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("attendees", "0002_accountvo"),
    ]

    operations = [
        migrations.AddField(
            model_name="accountvo",
            name="email",
            field=models.EmailField(default=1, max_length=254),
            preserve_default=False,
        ),
    ]
